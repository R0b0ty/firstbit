import sys


def Hello(name):
    if name == "first":
        nameSelected = "Daly"
    elif name == "last":
        nameSelected = "Gutierrez"
    else:
        nameSelected = "Unknown"
    print("Hello", nameSelected)
    return


# Define a main() function that prints a little greeting.
def main():
    Hello(sys.argv[1])


# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
    main()
